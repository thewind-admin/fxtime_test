<?php

/*
 * Routes:
 * /(books|pens|notebooks)      GET
 * /search [query]              GET     (save query into tags table)
 * /(books|pens|notebooks)/{ID}    GET
 * /tags
 *
 */

define("ROOT_DIR", __DIR__);

require_once ROOT_DIR . '/autoload.php';

$controller = new \Controller($_SERVER['REQUEST_URI']);
