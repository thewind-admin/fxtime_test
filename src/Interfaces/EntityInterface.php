<?php

namespace Interfaces;

/**
 * Interface EntityInterface
 * @package Interfaces
 */
interface EntityInterface
{

    /**
     * Returns the list of entity's fields, used for search
     *
     * @return array
     */
    public function getSearchableFields();

    /**
     * Stores new item with pre-validation
     *
     * @param array $params
     *
     * @return mixed
     */
    public function addItem(array $params);

}
