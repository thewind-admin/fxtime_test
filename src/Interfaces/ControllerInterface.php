<?php

namespace Interfaces;

interface ControllerInterface
{

    public function getActions();

}
