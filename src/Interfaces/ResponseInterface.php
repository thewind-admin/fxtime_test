<?php

namespace Interfaces;

/**
 * Interface ResponseInterface
 * @package Interfaces
 */
interface ResponseInterface
{

    public function error(string $text, string $description, string $httpCode);

    public function result(array $data, string $httpCode);

}
