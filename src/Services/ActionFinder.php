<?php

namespace Services;

/**
 * Class ActionFinder
 * @package Services
 */
class ActionFinder
{

    /**
     * @param array $actions
     * @param string $path
     *
     * @return array
     */
    public function findActionByPath(array $actions, string $path)
    {
        foreach ($actions as $actionUrl => $actionData) {
            $actionRequestMethod = 'GET';

            if (gettype($actionData) == 'array') {
                if (isset($actionData['name'])) {
                    $actionMethod = $actionData['name'];
                }
                if (isset($actionData['method'])) {
                    $actionRequestMethod = $actionData['method'];
                }
                if (isset($actionData['path'])) {
                    $actionUrl = $actionData['path'];
                }
            } else {
                $actionMethod = $actionData;
            }

            if (empty($actionMethod)) {
                continue;
            }

            $checkResult = preg_match($this->createPregPattern($actionUrl), $path, $matches);

            if ((bool)$checkResult === true) {
                if (!$actionRequestMethod || $actionRequestMethod == $_SERVER['REQUEST_METHOD']) {
                    return [$actionMethod, array_slice($matches, 1)];
                }
            }
        }

        return [null, []];
    }

    /**
     * @param string $str
     *
     * @return string
     */
    private function createPregPattern(string $str)
    {
        return "/^" . str_replace(['/'], ['\/'], $str) . "$/";
    }

}
