<?php

namespace Services;

use PDO;

/**
 * Class Db, singleton
 * @package Services
 */
class Db
{

    /**
     * @var Db
     */
    private static $instance;

    /**
     * @var PDO
     */
    private $connection;

    /**
     * Db constructor
     */
    private function __construct()
    {
        require_once ROOT_DIR . '/config.php';

        $this->connection = new PDO('mysql:host=' . DB_HOST . ';port=' . DB_PORT . ';dbname=' . DB_DATABASE . ';charset=utf8', DB_USERNAME, DB_PASSWORD);
    }

    /**
     * @return Db
     */
    private function __clone()
    {
        return self::getInstance();
    }

    /**
     * @return Db
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Redirect any methods to internal PDO interface
     *
     * @param string $method
     * @param array $args
     *
     * @return mixed
     */
    public function __call(string $method, array $args)
    {
        return call_user_func_array([$this->connection, $method], $args);
    }

}
