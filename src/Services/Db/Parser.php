<?php

namespace Services\Db;

use Services\Db;
use PDO;

/**
 * Class Parser
 * @package Services\Db
 */
class Parser
{

    /**
     * @param string $query
     * @param array $params
     *
     * @return mixed
     */
    protected function getSqlList(string $query, array $params = [])
    {
        $statement = $this->runQuery($query, $params);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $query
     * @param array $params
     *
     * @return mixed
     */
    protected function getSqlRow(string $query, array $params = [])
    {
        $statement = $this->runQuery($query, $params);

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $query
     * @param array $params
     *
     * @return mixed
     */
    public function addRow(string $query, array $params)
    {
        $this->runQuery($query, $params);

        return Db::getInstance()->lastInsertId();
    }

    /**
     * @param string $query
     * @param array $params
     *
     * @return mixed
     */
    private function runQuery(string $query, array $params = [])
    {
        $statement = Db::getInstance()->prepare($query);
        $statement->execute($params);

        return $statement;
    }

}
