<?php

namespace Services\Db;

/**
 * Class Set
 * @package Services\Db
 */
class Set extends Parser
{

    /**
     * @param string $className
     * @param bool $objectOriented
     *
     * @return array
     */
    public function findAll(string $className, bool $objectOriented = false)
    {
        $list = $this->getSqlList(sprintf($className::SQL_QUERY_SELECT_LIST, ''));

        $classObj = new $className;
        $outputFields = array_flip($classObj->getOutputFields());

        $list = array_map(function ($row) use ($outputFields) {
            return array_intersect_key($row, $outputFields);
        }, $list);

        return $list;
    }

    /**
     * @param string $className
     * @param int $id
     *
     * @return mixed
     */
    public function findOne(string $className, int $id)
    {
        $row = $this->getSqlRow(sprintf($className::SQL_QUERY_SELECT_LIST, "WHERE `entity_table`.`id`=?"), [$id]);

        return $row;
    }

    /**
     * @param string $className
     * @param string $query
     *
     * @return array
     */
    public function findByQuery(string $className, string $query)
    {
        $classObj = new $className;
        $textFields = $classObj->getSearchableFields();

        $textFieldsQuery = implode(' OR ', array_map(function ($field) use ($textFields) {
            return (!empty($textFields[$field]['where_alias']) ? $textFields[$field]['where_alias'] : "`entity_table`.`{$field}`") . " LIKE ?";
        }, array_keys($textFields)));

        $textFieldsParams = array_fill(0, sizeOf($textFields), '%' . $query . '%');

        $list = $this->getSqlList(sprintf($className::SQL_QUERY_SELECT_LIST, "WHERE " . $textFieldsQuery), $textFieldsParams);

        return $list;
    }

}
