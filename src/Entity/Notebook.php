<?php

namespace Entity;

use Interfaces\EntityInterface;

/**
 * Class Notebook
 * @package Entity
 */
class Notebook extends AbstractEntity implements EntityInterface
{

    /**
     * Fields of entity
     * @var array
     */
    protected $fields = [
        'id' => ['type' => 'int', 'nullable' => true],
        'manufacturer' => ['type' => 'string', 'required' => true],
        'vendor_code' => ['type' => 'string', 'required' => true],
        'cover_type' => ['type' => 'string', 'required' => true, 'values' => ['hard', 'soft']],
    ];

    /**
     * @var array
     */
    protected $outputFields = ['manufacturer', 'cover_type'];

    /**
     * Template of list query for entity
     * %s must be formatted with WHERE substring
     */
    const
        SQL_QUERY_SELECT_LIST = "
            SELECT `entity_table`.*
            FROM `notebook` `entity_table`
            %s
            ORDER BY `entity_table`.`id` DESC
        ",
        SQL_QUERY_INSERT_ITEM = "
            INSERT INTO `notebook` (`id`, `manufacturer`, `vendor_code`, `cover_type`) VALUES ({%id%}, {%manufacturer%}, {%vendor_code%}, {%cover_type%})
        ";

}
