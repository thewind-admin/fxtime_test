<?php

namespace Entity;

use Interfaces\EntityInterface;

/**
 * Class Tag
 * @package Entity
 */
class Tag extends AbstractEntity implements EntityInterface
{

    /**
     * Fields of entity
     * @var array
     */
    protected $fields = [
        'id' => ['type' => 'int', 'nullable' => true],
        'text' => ['type' => 'string', 'required' => true],
        'count' => ['type' => 'int', 'min_length' => 1],
    ];

    /**
     * @var array
     */
    protected $outputFields = ['text', 'count'];

    /**
     * Template of list query for entity
     * %s must be formatted with WHERE substring
     */
    const
        SQL_QUERY_SELECT_LIST = "
            SELECT `entity_table`.*
            FROM `tag` `entity_table`
            %s
            ORDER BY `entity_table`.`count` DESC
        ",
        SQL_QUERY_INSERT_ITEM = "
            INSERT IGNORE INTO `tag` (`id`, `text`, `count`) VALUES ({%id%}, {%text%}, {%count%}) ON DUPLICATE KEY UPDATE `count`=`count`+{%count%}
        ";

}
