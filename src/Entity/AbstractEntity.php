<?php

namespace Entity;

use Services\Db\Set;

/**
 * Class AbstractEntity
 * @package Entity
 */
abstract class AbstractEntity
{

    const
        ERROR_CODE_REQUIRED = 1,
        ERROR_CODE_VALUES = 2,

        TEXT_NOT_ENOUGH_REQUIRED_FIELDS = 'Not enough required fields';

    /**
     * @return array
     */
    public function getOutputFields()
    {
        if (property_exists($this, 'outputFields')) {
            return $this->outputFields;
        } else {
            return array_keys($this->fields);
        }
    }

    /**
     * @return array
     */
    public function getSearchableFields()
    {
        return $this->getFieldsByParams('searchable', true);
    }

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function addItem(array $params)
    {
        $requiredFieldsValidation = $this->checkRequiredFields($params);
        if ($requiredFieldsValidation !== true) {
            return [self::ERROR_CODE_REQUIRED, self::TEXT_NOT_ENOUGH_REQUIRED_FIELDS];
        }

        list ($valuesFieldsValidation, $paramsData) = $this->checkValuesFields($params);
        if ($valuesFieldsValidation !== true) {
            return [self::ERROR_CODE_VALUES, $paramsData];
        }

        $paramsData['id'] = null;

        $sqlQuery = static::SQL_QUERY_INSERT_ITEM;
        $sqlParams = [];

        foreach ($this->fields as $k => $v) {
            $v = $paramsData[$k];

            $p = 0;

            $paramPattern = '{%' . $k . '%}';

            while (($p = stripos($sqlQuery, $paramPattern, $p)) !== false) {
                $sqlParams[] = $v;
                $p++;
            }

            $sqlQuery = str_replace($paramPattern, '?', $sqlQuery);
        }

        $set = new Set();
        $id = $set->addRow($sqlQuery, $sqlParams);

        return [true, $id];
    }

    /**
     * @param string $key
     * @param mixed $type
     *
     * @return array
     */
    private function getFieldsByParams(string $key, $type)
    {
        return array_filter($this->fields, function ($field) use ($key, $type) {
            return isset($field[$key]) && $field[$key] === $type;
        });
    }

    /**
     * @param array $params
     *
     * @return bool
     */
    private function checkRequiredFields(array $params)
    {
        $requiredFields = array_filter($this->fields, function ($field) {
            return !empty($field['required']);
        });

        return sizeOf(array_intersect_key($params, $requiredFields)) == sizeOf($requiredFields);
    }

    /**
     * @param array $params
     *
     * @return array|
     */
    private function checkValuesFields(array $params)
    {
        $params = array_intersect_key($params, $this->fields);

        $errorParams = [];

        foreach ($params as $k => $v) {
            $field = $this->fields[$k];

            if (!empty($field['type'])) {
                settype($v, $field['type']);
            }

            if (empty($field['nullable']) && empty($v)) {
                $errorParams[$k][] = 'empty';
            }

            if (isset($field['min_length']) && strlen((string)$v) < $field['min_length']) {
                $errorParams[$k][] = 'min_length';
            }

            if (isset($field['max_length']) && strlen((string)$v) < $field['max_length']) {
                $errorParams[$k][] = 'max_length';
            }

            if (isset($field['values']) && !in_array($v, $field['values'])) {
                $errorParams[$k][] = 'wrong_value';
            }

            $params[$k] = $v;
        }

        if (empty($errorParams)) {
            return [true, $params];
        }

        return [false, $errorParams];
    }

}
