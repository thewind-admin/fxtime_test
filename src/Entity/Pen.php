<?php

namespace Entity;

use Interfaces\EntityInterface;

/**
 * Class Pen
 * @package Entity
 */
class Pen extends AbstractEntity implements EntityInterface
{

    /**
     * Fields of entity
     * @var array
     */
    protected $fields = [
        'id' => ['type' => 'int', 'nullable' => true],
        'manufacturer' => ['type' => 'string', 'required' => true],
        'vendor_code' => ['type' => 'string', 'required' => true],
        'color' => ['type' => 'string', 'required' => true],
    ];

    /**
     * @var array
     */
    protected $outputFields = ['manufacturer', 'color'];

    /**
     * Template of list query for entity
     * %s must be formatted with WHERE substring
     */
    const
        SQL_QUERY_SELECT_LIST = "
            SELECT `entity_table`.*
            FROM `pen` `entity_table`
            %s
            ORDER BY `entity_table`.`id` DESC
        ",
        SQL_QUERY_INSERT_ITEM = "
            INSERT INTO `pen` (`id`, `manufacturer`, `vendor_code`, `color`) VALUES ({%id%}, {%manufacturer%}, {%vendor_code%}, {%color%})
        ";

}
