<?php

namespace Entity;

use Interfaces\EntityInterface;

/**
 * Class Book
 * @package Entity
 */
class Book extends AbstractEntity implements EntityInterface
{

    /**
     * Fields of entity
     * @var array
     */
    protected $fields = [
        'id' => ['type' => 'int', 'nullable' => true],
        'name' => ['type' => 'string', 'searchable' => true, 'required' => true],
        'year' => ['type' => 'int', 'min_length' => 4, 'max_length' => 4, 'required' => true],
        'isbn' => ['type' => 'string', 'searchable' => true, 'required' => true],
        'authors' => ['type' => 'string', 'searchable' => true, 'where_alias' => 'a.`name`'],
    ];

    /**
     * @var array
     */
    protected $outputFields = ['name', 'authors', 'isbn'];

    /**
     * Template of list query for entity
     * %s must be formatted with WHERE substring
     */
    const
        SQL_QUERY_SELECT_LIST = "
            SELECT `entity_table`.*, GROUP_CONCAT(a.`name` ORDER BY a.`id` ASC SEPARATOR ', ') as `authors`
            FROM `book` `entity_table`
            LEFT JOIN `book_to_author` ba ON (ba.`book_id` = `entity_table`.`id`)
            LEFT JOIN `author` a ON (a.`id` = ba.`author_id`)
            %s
            GROUP BY `entity_table`.`id`
            ORDER BY `entity_table`.`id` DESC
        ",
        SQL_QUERY_INSERT_ITEM = "
            INSERT INTO `book` (`id`, `name`, `year`, `isbn`) VALUES ({%id%}, {%name%}, {%year%}, {%isbn%})
        ";

}
