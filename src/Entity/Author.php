<?php

namespace Entity;

use Interfaces\EntityInterface;
use Services\Db\Set;

/**
 * Class Author
 * @package Entity
 */
class Author extends AbstractEntity implements EntityInterface
{

    /**
     * Fields of entity
     * @var array
     */
    protected $fields = [
        'id' => ['type' => 'int', 'nullable' => true],
        'name' => ['type' => 'string', 'required' => true],
    ];

    /**
     * Template of list query for entity
     * %s must be formatted with WHERE substring
     */
    const
        SQL_QUERY_SELECT_LIST = "
            SELECT `entity_table`.*
            FROM `author` `entity_table`
            %s
            ORDER BY `entity_table`.`id` ASC
        ",
        SQL_QUERY_INSERT_ITEM = "
            INSERT IGNORE INTO `author` (`id`, `name`) VALUES ({%id%}, {%name%})
        ";

    /**
     * Connects books and authors
     *
     * @param int $authorId
     * @param int $bookId
     */
    public function connectToBook(int $authorId, int $bookId)
    {
        $sqlQuery = "INSERT IGNORE INTO `book_to_author` (`book_id`,`author_id`) VALUES (?,?)";
        $sqlParams = [$bookId, $authorId];

        $set = new Set();
        $set->addRow($sqlQuery, $sqlParams);
    }

}
