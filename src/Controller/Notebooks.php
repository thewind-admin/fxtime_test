<?php

namespace Controller;

use Entity\Notebook;
use Interfaces\ControllerInterface;
use Services\Db\Set;

/**
 * Class Notebooks
 * @package Controller
 */
class Notebooks extends Section implements ControllerInterface
{

    /**
     * List of actions supported by this class
     * Keys of the list are regexp, whose matches are used as parameters
     *
     * 2 possible formats: {path} => {actionName}
     * or [path => {path}, name => {actionName}[, method => {requestMethod}]]
     *
     * @var array
     */
    protected $actions = [
        '/' => 'listAction',
        ['path' => '/', 'name' => 'createAction', 'method' => 'POST'],
        '/([1-9]+[0-9]*)' => 'itemAction',
    ];

    /**
     * Returns list of notebooks
     *
     * @return string
     */
    public function listAction()
    {
        $set = new Set();
        $notebookList = $set->findAll(Notebook::class);

        return $this->response->result([
            'list' => $notebookList
        ]);
    }

    /**
     * Returns details about notebook
     *
     * @param int $notebook_id
     *
     * @return string
     */
    public function itemAction(int $notebook_id)
    {
        $set = new Set();
        $notebook = $set->findOne(Notebook::class, $notebook_id);

        if (!isset($notebook['id'])) {
            return $this->response->error(static::TEXT_ITEM_NOT_FOUND, 'Notebook id is wrong');
        }

        return $this->response->result([
            'notebook' => $notebook,
        ]);
    }

    /**
     * Creates new item
     *
     * @return string
     */
    public function createAction()
    {
        $obj = new Notebook();
        list ($result, $data) = $obj->addItem($_POST);

        if ($result !== true) {
            return $this->response->error(static::TEXT_ADD_ERROR, json_encode($data));
        }

        return $this->response->result(['id' => $data]);
    }

}
