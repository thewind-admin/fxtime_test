<?php

namespace Controller;

use Response\Json as Response;

/**
 * Class Section
 * @package Controller
 */
abstract class Section
{

    const
        TEXT_ITEM_NOT_FOUND = 'Not found',
        TEXT_ADD_ERROR = 'Adding item error'
    ;

    /**
     * @var Response
     */
    protected $response;

    /**
     * Section constructor.
     */
    public function __construct()
    {
        $this->response = new Response();
    }

    /**
     * Returns the list of supported actions
     *
     * @return array
     */
    public function getActions()
    {
        return $this->actions;
    }

}
