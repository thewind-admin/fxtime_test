<?php

namespace Controller;

use Entity\Author;
use Entity\Book;
use Interfaces\ControllerInterface;
use Services\Db\Set;

/**
 * Class Books
 * @package Controller
 */
class Books extends Section implements ControllerInterface
{

    /**
     * List of actions supported by this class
     * Keys of the list are regexp, whose matches are used as parameters
     *
     * 2 possible formats: {path} => {actionName}
     * or [path => {path}, name => {actionName}[, method => {requestMethod}]]
     *
     * @var array
     */
    protected $actions = [
        '/' => 'listAction',
        ['path' => '/', 'name' => 'createAction', 'method' => 'POST'],
        '/([1-9]+[0-9]*)' => 'itemAction',
    ];

    /**
     * Returns list of books
     *
     * @return string
     */
    public function listAction()
    {
        $set = new Set();
        $bookList = $set->findAll(Book::class);

        return $this->response->result([
            'list' => $bookList
        ]);
    }

    /**
     * Returns details about book
     *
     * @param int $book_id
     *
     * @return string
     */
    public function itemAction(int $book_id)
    {
        $set = new Set();
        $book = $set->findOne(Book::class, $book_id);

        if (!isset($book['id'])) {
            return $this->response->error(static::TEXT_ITEM_NOT_FOUND, 'Book id is wrong');
        }

        return $this->response->result([
            'book' => $book,
        ]);
    }


    /**
     * Creates new item
     *
     * @return string
     */
    public function createAction()
    {
        //print_r($_POST); die(PHP_EOL);

        $obj = new Book();
        list ($result, $data) = $obj->addItem($_POST);

        if ($result !== true) {
            return $this->response->error(static::TEXT_ADD_ERROR, json_encode($data));
        }

        $bookId = (int)$data;

        if (!empty($_POST['authors'])) {
            $obj = new Author();

            foreach ($_POST['authors'] as $author) {
                list ($result, $data) = $obj->addItem(['name' => $author]);

                if ($result === true) {
                    $obj->connectToBook((int)$data, $bookId);
                }
            }
        }

        return $this->response->result(['id' => $bookId]);
    }

}
