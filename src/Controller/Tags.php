<?php

namespace Controller;

use Entity\Tag;
use Interfaces\ControllerInterface;
use Services\Db\Set;

/**
 * Class Tags
 * @package Controller
 */
class Tags extends Section implements ControllerInterface
{

    /**
     * List of actions supported by this class
     * Keys of the list are regexp, whose matches are used as parameters
     *
     * 2 possible formats: {path} => {actionName}
     * or [path => {path}, name => {actionName}[, method => {requestMethod}]]
     *
     * @var array
     */
    protected $actions = [
        '/' => 'listAction',
    ];

    /**
     * Returns list of tags
     *
     * @return string
     */
    public function listAction()
    {
        $set = new Set();
        $tagList = $set->findAll(Tag::class);

        return $this->response->result([
            'list' => $tagList
        ]);
    }

}
