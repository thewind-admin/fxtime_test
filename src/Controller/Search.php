<?php

namespace Controller;

use Entity\Book;
use Entity\Notebook;
use Entity\Pen;
use Entity\Tag;
use Interfaces\ControllerInterface;
use Services\Db\Set;

/**
 * Class Tags
 * @package Controller
 */
class Search extends Section implements ControllerInterface
{

    const TEXT_EMPTY_QUERY = 'Empty search query';

    /**
     * List of actions supported by this class
     * Keys of the list are regexp, whose matches are used as parameters
     *
     * 2 possible formats: {path} => {actionName}
     * or [path => {path}, name => {actionName}[, method => {requestMethod}]]
     *
     * @var array
     */
    protected $actions = [
        '/(.*)' => 'listAction',
    ];

    /**
     * Returns list of tags
     *
     * @param string $query
     *
     * @return string
     */
    public function listAction(string $query)
    {
        $query = trim(urldecode($query));

        if (empty($query)) {
            return $this->response->error(self::TEXT_EMPTY_QUERY);
        }

        // save query
        $tag = new Tag();
        $tag->addItem(['text' => $query, 'count' => 1]);

        $set = new Set();

        $searchEntities = [
            'book' => Book::class,
            'pen' => Pen::class,
            'notebook' => Notebook::class,
        ];

        $resultList = [];

        foreach ($searchEntities as $type => $className) {
            $resultList = array_merge($resultList, array_map(function ($v) use ($type) {
                $v['type'] = $type;
                return $v;
            }, $set->findByQuery($className, $query)));
        }

        return $this->response->result([
            'list' => $resultList
        ]);
    }

}
