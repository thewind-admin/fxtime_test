<?php

namespace Controller;

use Entity\Pen;
use Interfaces\ControllerInterface;
use Services\Db\Set;

/**
 * Class Pens
 * @package Controller
 */
class Pens extends Section implements ControllerInterface
{

    /**
     * List of actions supported by this class
     * Keys of the list are regexp, whose matches are used as parameters
     *
     * 2 possible formats: {path} => {actionName}
     * or [path => {path}, name => {actionName}[, method => {requestMethod}]]
     *
     * @var array
     */
    protected $actions = [
        '/' => 'listAction',
        ['path' => '/', 'name' => 'createAction', 'method' => 'POST'],
        '/([1-9]+[0-9]*)' => 'itemAction',
    ];

    /**
     * Returns list of pens
     *
     * @return string
     */
    public function listAction()
    {
        $set = new Set();
        $penList = $set->findAll(Pen::class);

        return $this->response->result([
            'list' => $penList
        ]);
    }

    /**
     * Returns details about pen
     *
     * @param int $pen_id
     *
     * @return string
     */
    public function itemAction(int $pen_id)
    {
        $set = new Set();
        $pen = $set->findOne(Pen::class, $pen_id);

        if (!isset($pen['id'])) {
            return $this->response->error(static::TEXT_ITEM_NOT_FOUND, 'Pen id is wrong');
        }

        return $this->response->result([
            'pen' => $pen,
        ]);
    }


    /**
     * Creates new item
     *
     * @return string
     */
    public function createAction()
    {
        $obj = new Pen();
        list ($result, $data) = $obj->addItem($_POST);

        if ($result !== true) {
            return $this->response->error(static::TEXT_ADD_ERROR, json_encode($data));
        }

        return $this->response->result(['id' => $data]);
    }

}
