<?php

use Services\ActionFinder;
use Response\Json as Response;

class Controller
{

    const
        EMPTY_CONTROLLER = 'No API path passed',
        WRONG_CONTROLLER = 'API does not contain passed path',
        API_INTERNAL_ERROR = 'API internal error';

    /**
     * Controller constructor.
     *
     * @param string $uri
     */
    public function __construct(string $uri)
    {
        $response = new Response;

        $uriParts = parse_url($uri);

        $path = trim($uriParts['path'], '/');
        if (!$path) {
            return $response->error(self::EMPTY_CONTROLLER);
        }

        $pathParts = explode('/', $path);
        $controllerName = $pathParts[0];

        try {
            $controllerName = 'Controller\\' . ucfirst(strtolower($controllerName));
            $controllerClass = new $controllerName;
        } catch (Exception $e) {
            return $response->error(self::WRONG_CONTROLLER, $e->getMessage());
        }

        $controllerActions = $controllerClass->getActions();
        $pathAction = '/' . implode('/', array_slice($pathParts, 1));

        $actionFinder = new ActionFinder();
        list($controllerAction, $controllerActionParams) = $actionFinder->findActionByPath($controllerActions, $pathAction);

        if (!$controllerAction) {
            return $response->error(self::API_INTERNAL_ERROR, 'No action for passed path.');
        }

        if (!method_exists($controllerClass, $controllerAction)) {
            return $response->error(self::API_INTERNAL_ERROR, 'Action for passed path found, but not implemented');
        }

        return call_user_func_array([$controllerClass, $controllerAction], $controllerActionParams);
    }

}
