<?php

namespace Response;

use Interfaces\ResponseInterface;

class Json extends AbstractResponse implements ResponseInterface
{

    protected function response(string $httpStatus, array $data = [])
    {
        header("HTTP/1.0 " . $httpStatus);
        echo json_encode($data);
        return true;
    }

}