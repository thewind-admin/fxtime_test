<?php

namespace Response;

use Interfaces\ResponseInterface;

/**
 * Class AbstractResponse
 * @package Response
 */
abstract class AbstractResponse implements ResponseInterface
{

    const
        HTTP_OK = '200 OK',
        HTTP_FOUND = '302 Found',
        HTTP_MOVED = '301 Moved permanently',
        HTTP_NOT_FOUND = '404 Not Found';

    /**
     * @param string $text
     * @param string|null $description
     * @param string $httpCode
     *
     * @return string
     */
    public function error(string $text, string $description = null, string $httpCode = self::HTTP_NOT_FOUND)
    {
        return $this->response($httpCode, [
            'error' => $text,
            'error_description' => $description,
        ]);
    }

    /**
     * @param array $data
     * @param string $httpCode
     *
     * @return string
     */
    public function result(array $data, string $httpCode = self::HTTP_OK)
    {
        return $this->response($httpCode, $data);
    }

    /**
     * @param string $httpStatus
     * @param array $data
     *
     * @return string
     */
    abstract protected function response(string $httpStatus, array $data = []);

}
