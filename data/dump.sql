
CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `author` (`id`, `name`) VALUES
(1, 'Super writer');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `year` int(4) NOT NULL,
  `isbn` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `book` (`id`, `name`, `year`, `isbn`) VALUES
(1, 'My first book', 2017, '9538548503453');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `book_to_author` (
  `book_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  UNIQUE KEY `book_id` (`book_id`,`author_id`),
  KEY `book_id_2` (`book_id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `book_to_author` (`book_id`, `author_id`) VALUES
(1, 1);

ALTER TABLE `book_to_author`
  ADD CONSTRAINT `book_to_author_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`),
  ADD CONSTRAINT `book_to_author_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`);

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `notebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` varchar(255) NOT NULL,
  `vendor_code` varchar(100) NOT NULL,
  `cover_type` enum('hard','soft') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `notebook` (`id`, `manufacturer`, `vendor_code`, `cover_type`) VALUES
(1, 'NTB WorldManufacturer', 'ntb_wrld_man', 'soft');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `pen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` varchar(255) NOT NULL,
  `vendor_code` varchar(100) NOT NULL,
  `color` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `pen` (`id`, `manufacturer`, `vendor_code`, `color`) VALUES
(1, 'Pen provider', 'pen_prov_1', 'black');

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `text` (`text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
