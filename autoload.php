<?php

spl_autoload_register(function ($className) {

    $className = str_replace('\\', '/', trim($className, '\\'));

    $classFilePath = __DIR__ . '/src/' . $className . '.php';

    if (!file_exists($classFilePath))
        throw new Exception('Section not found');

    include_once $classFilePath;

    return true;

});
