# Test API

API implements lists / items of the following entities: books, pens, notebooks

Test server: http://fxtm.dignatiev.ru

## Lists

- **Books** curl -X GET http://fxtm.dignatiev.ru/books
- **Notebooks** curl -X GET http://fxtm.dignatiev.ru/notebooks
- **Pens** curl -X GET http://fxtm.dignatiev.ru/pens
- **Search tags** curl -X GET http://fxtm.dignatiev.ru/tags

## Details of item

- curl -X GET http://fxtm.dignatiev.ru/books/1
- curl -X GET http://fxtm.dignatiev.ru/notebooks/1
- curl -X GET http://fxtm.dignatiev.ru/pens/1

## Search

- curl -X GET http://fxtm.dignatiev.ru/search/first


## Adding items

curl -X POST http://fxtm.dignatiev.ru/books --data "name=Test%20Book&year=2017&isbn=5875437598&authors%5B%5D=Ivanov%20Vasya"

curl -X POST http://fxtm.dignatiev.ru/notebooks --data "manufacturer=Test%20Man&vendor_code=test_man_1&cover_type=soft"

curl -X POST http://fxtm.dignatiev.ru/pens --data "manufacturer=Pen%20Provider&vendor_code=pen_prov_1&color=black"
