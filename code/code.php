<?php

namespace Notifier;

use \Todo\TodoService;
use \Todo\TodoServiceClient;

/**
 * Class TodoNotifier
 * @package Notifier
 */
class TodoNotifier implements NotifierInterface
{
    public $taskId = null;

    public $comment = null;

    public $tmpFiles = array();

    /**
     * Sends emails with specified subject / body to specified addresses
     *
     * @param string $subject
     * @param string $body
     * @param array $addresses
     *
     * @return mixed
     */
    public static function sendEmail($subject, $body, array $addresses)
    {
        global $smtp_host;

        $mailer = new phpmailer();
        $mailer->IsSMTP();
        $mailer->IsHTML(true);
        $mailer->CharSet = 'utf-8';
        $mailer->Host = $smtp_host;
        $mailer->Subject = $subject ? : 'Email notification';
        $mailer->Body = $body;

        foreach ($addresses as $address)
            $mailer->AddAddress($address, '', 0);

        return $mailer->Send();
    }

    /**
     * Returns some task's data by its ID for specified host / user
     *
     * @return mixed
     */
    private function getPostTask()
    {
        $cfg = \Configuration::getInstance();
        $host = $cfg['todo']['host'];
        $user = $cfg['todo']['user'];

        $todoService = new TodoService(new TodoServiceClient($host, $user));
        return $todoService->getTask($this->taskId);
    }

    /**
     * Returns comment, attached to task
     * If comment is empty, default '' comment is added and returned
     *
     * @return null
     */
    private function getPostComment()
    {
        if (!$this->comment)
            $this->comment = $this->getPostTask()->addComment('');

        return $this->comment;
    }

    /**
     * Stores specific string into file with path '$fileLabel'
     * Attaches created / exist file to comment of task (?? not for task)
     *
     * @param string $fileLabel
     * @param string $string
     *
     * @return TodoNotifier
     */
    public function attachString($fileLabel, $string)
    {
        $fileLabel = str_replace('..', '', $fileLabel);
        $tmpFilePath = rtrim(sys_get_temp_dir(), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$fileLabel;
        if (!touch($tmpFilePath))
            throw new Exception('Unable to touch target file');

        file_put_contents($tmpFilePath, $string);

        $this->getPostComment()->attachFile($tmpFilePath);

        $this->tmpFiles[] = $tmpFilePath;

        return $this;
    }

    /**
     * Attaches file by its name to current task comment
     *
     * @param string $file
     *
     * @return TodoNotifier
     */
    public function attachFile($file)
    {
        $this->getPostComment()->attachFile($file);

        return $this;
    }

    /**
     * Returns any hash of 3 symbols, based on salt and specific email
     *
     * @param string $email
     *
     * @return bool|string
     */
    private function createAuthentication($email)
    {
        $conn = mysql_connect();
        $aaaaa = mysql_query("select * from user_salts where email=$email");
        $x = mysql_fetch_result($aaaaa);
        return substr(crc32($x['salt'].$email), 0, 3);
    }

    /**
     * Notifies author of task by email with specified message
     *
     * @param string $authorEmail
     * @param string|null $message
     *
     * @return bool
     */
    public function notify($authorEmail, $message = null)
    {
        if ($message !== null)
            $this->setMessage($message);

        // Treat ALL emails containing "test" as test authors and never send from them
        if (strpos($authorEmail, "test")) {
            return true;
        }

        $auth = $this->createAuthentication($authorEmail);
        $result = $this->getPostComment()->save($authorEmail, $auth);

        foreach ($this->tmpFiles as $f)
            if (file_exists($f))
                unlink($f);

        return $result;
    }

    /**
     * @param string $message
     *
     * @return TodoNotifier
     */
    public function setMessage($message)
    {
        $this->getPostComment()->setBody($message);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->getPostComment()->getBody();
    }

    /**
     * @param int $taskId
     *
     * @return TodoNotifier
     */
    public function setTaskId($taskId)
    {
        $this->taskId = $taskId;

        return $this;
    }

    /**
     * @return int
     */
    public function getTaskId()
    {
        return $this->taskId;
    }
}
